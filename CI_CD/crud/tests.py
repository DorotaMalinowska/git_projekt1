from django.test import TestCase
from django.test import Client

from .models import Message


# Create your tests here.
class MessageTestCase(TestCase):

    def setUp(self):
        m1 = Message.objects.create(
            name="Jan Kołodziej",
            email="j.kolodziej@vp.pl",
            priority=4,
            category="question",
            subject="Racz Waść wpaść!",
            body="Racz Waść wpaść!",
        )

        m2 = Message.objects.create(
            name="Ewa Kasztelan",
            email="j.kasztelan@vp.pl",
            priority=9,
            category="question",
            subject="Przyjdę",
            body="Puste",

        )

        m3 = Message.objects.create(
            name="Jan Tyskie",
            email="j.tyskie@vp.pl",
            priority=43,
            category="question",
            subject="Moja babiczka pochodzi z Chrzanowa",
            body="Moja babiczka pochodzi z Chrzanowa",

        )

    ### Testowanie modelu znajdujacego sie na serwerze
    def test_create_object(self):
        lenght = len(Message.objects.all())
        self.assertEqual(lenght, 3)

    def test_valid_message(self):  # testujemy model, czy potrafi sie zwalidowac.
        m = Message.objects.get(id=1)
        self.assertTrue(m.is_valid_message())

    def test_invalid_message(self):
        m = Message.objects.filter(name="Jan Tyskie").first()
        self.assertFalse(m.is_valid_message())

    def test_increase_priority(self):
        m = Message.objects.get(id=2)
        p = m.priority
        m.increase_priority()
        self.assertEqual(p + 1, m.priority)

    def test_set_priority(self):
        m = Message.objects.get(id=1)
        # p = m.priority
        m.set_priority(9)
        # self.assertNotEqual(p, 9)
        self.assertEqual(m.priority, 9)

    # Testowanie odpowiedzi serwera

    def test_messages_response(self):
        c = Client()
        response = c.get("/crud/message-list")
        self.assertEqual(response.status_code, 200)

    def test_messages_invalid_response(self):
        c = Client()
        response = c.get("/crud/message-list/5")
        self.assertEqual(response.status_code, 404)

    def test_messages_context(self):
        c = Client()
        response = c.get("/crud/message-list")
        context = response.context['object_list']
        self.assertEqual(len(context), 3)

    def test_messages_detail_response(self):
        c = Client()
        response = c.get("/crud/message-detail/3")
        self.assertEqual(response.context['object'].email, "j.tyskie@vp.pl")
        self.assertEqual(response.status_code, 200)

        # ja zapisałam to tak:
        # context = response.context['object']
        # email = context.email
        # self.assertEqual(email, "j.tyskie@vp.pl")

    def test_message_form(self):
        c = Client()
        response = c.post('/crud/message-create', {
            'name': "Katarzyna Dowbor",
            'email': "k.dowbor@vp.pl",
            'priority': 4,
            'category': "question",
            'subject' : "O co cho?",
            'body': "sie nie dowiesz",
        })
        self.assertEqual(response.status_code, 302)
        m = Message.objects.all()
        self.assertEqual(m.count(), 4)
