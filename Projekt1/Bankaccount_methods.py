from Bankaccount_class import BankAccount


def activate(self):
    self.if_active = True
    return 'Your account has been activated'


def close(self):
    self.if_active = False
    return "Your account has been closed."


def saldo(self):
    return f"You have {self.balance}$ on your account"


def deposit(self, amount):
    self.balance += amount
    return f"You have {self.balance}$ on your account"


def withdrawal(self, amount):
    if amount <= self.balance:
        self.balance -= amount
        return f"Your money have been withdrawn"
    else:
        return f"You don't have enough money on your account to withdraw {amount}$"
    return self.balance

Dorota = BankAccount('Dorota', 'Malinowska')
# print(Dorota.activate())
print(Dorota.saldo())
print(Dorota.deposit(300))
print(Dorota.saldo())
print(Dorota.withdrawal(500))
print(Dorota.withdrawal(100))
print(Dorota.saldo())
